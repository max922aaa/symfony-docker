SHELL := /bin/bash

tests:
	php bin/console doctrine:database:drop --force --env=test || true
	php bin/console doctrine:database:create --env=test
	php bin/console doctrine:migrations:migrate -n --env=test
	php bin/console doctrine:fixtures:load -n --env=test
	php bin/phpunit $@
.PHONY: tests

up:
	docker-compose up -d

down:
	docker-compose down --remove-orphans

build:
	docker-compose build --no-cache

exec:
	docker-compose exec php bash
