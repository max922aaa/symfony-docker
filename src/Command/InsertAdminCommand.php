<?php

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;

#[AsCommand(
    name: 'app:create-admin',
    description: 'Creates a new user.',
    hidden: false,
    aliases: ['app:create-admin']
)]
class InsertAdminCommand extends Command
{
    private $connection;

    public function __construct(Connection $connection)
    {
        //$this->connection = $connection;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sql = "INSERT INTO admin (id, username, roles, password) VALUES (nextval('admin_id_seq')";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([
            'admin', 
            '["ROLE_ADMIN"]', 
            '$argon2id$v=19$m=65536,t=4,p=1$BQG+jovPcunctc30xG5PxQ$TiGbx451NKdo+g9vLtfkMy4KjASKSOcnNxjij4gTX1s'
        ]);

        $output->writeln('Admin успішно додано.');

       $output->writeln([
        '============',
        '============',
        'User Admin',
        '============',
        '============',
        '             ',
        '',
       ]);

       return Command::SUCCESS;
    }
}